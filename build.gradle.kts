import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {

	id("org.springframework.boot") version "2.5.4"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"

	war

	kotlin("jvm") version "1.5.21"
	kotlin("plugin.spring") version "1.5.21"
	kotlin("plugin.serialization") version "1.5.21"

}

group = "com.serwylo"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {

	implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

	providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
	
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

	implementation("org.jetbrains.kotlin:kotlin-stdlib:1.5.21")
	implementation("io.ktor:ktor-client-core:1.6.3")
	implementation("io.ktor:ktor-client-cio:1.6.3")
	implementation("io.ktor:ktor-client-serialization:1.6.3")

	implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.2.2")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.2")

	implementation("org.jsoup:jsoup:1.14.2")

	implementation("com.itextpdf:itextpdf:5.5.13.2")
	implementation("org.apache.pdfbox:pdfbox:2.0.24")

	implementation("com.sksamuel.scrimage:scrimage-core:4.0.22")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
